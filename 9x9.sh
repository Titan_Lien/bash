#!/bin/bash
for ((r=1;r<=9;r++))
do
	printout=''
	for((c=1;c<=9;c++))
	do
		result=$[${r} * ${c}]
		printout=${printout}${c}" * "${r}" ="`printf '%2i\n' ${result}`" "
	done
	echo "${printout}"
done

