#!/bin/bash
if [ x$1 = x ]; then
	find ./ -name '*.c' -o -name '*.h' -o -name '*.S' -o -name '*.cpp' -o -name '*.m' -o -name '*.y' -o -name '*.java' > ./cscope.files
	cscope -bqR && ctags -Rb
	CSCOPE_DB=./cscope.out; export CSCOPE_DB
else
	rm -rf ./cscope.* tags
fi

